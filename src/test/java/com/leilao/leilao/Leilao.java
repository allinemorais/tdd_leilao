package com.leilao.leilao;

import com.leilao.leilao.controller.LeilaoController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Leilao {

    LeilaoController leilaoController = new LeilaoController();

    @Test
    public void AdicionarLance()
    {

        LanceModel lanceModel = new LanceModel();
        UsuarioMoldel usuarioMoldel = new UsuarioMoldel();

        usuarioMoldel.setNome("Joaquin");
        usuarioMoldel.setId(3);


        lanceModel.setValorLance(38.0);
        lanceModel.setUsuario(usuarioMoldel);

        Assertions.assertEquals( leilaoController.AdicionarNovoLance(lanceModel),true);
    }

    @Test
    public void ValidarLance()
    {

        LanceModel lanceModel = new LanceModel();
        UsuarioMoldel usuarioMoldel = new UsuarioMoldel();

        usuarioMoldel.setNome("Maria");
        usuarioMoldel.setId(3);


        lanceModel.setValorLance(40.0);
        lanceModel.setUsuario(usuarioMoldel);

        Assertions.assertEquals( leilaoController.Validarlance(lanceModel),true);
    }

}
