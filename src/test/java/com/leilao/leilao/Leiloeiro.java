package com.leilao.leilao;

import com.leilao.leilao.controller.LeilaoController;
import com.leilao.leilao.controller.LeiloeiroController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Leiloeiro {

    @Test
    public void RetornarMaiorLance()
        {
            LeiloeiroController leiloeiroController = new LeiloeiroController();
            LeilaoModel leilaoModel = new LeilaoModel();
            double result = 23.0;


            Assertions.assertEquals( LeiloeiroController.RetornarMaiorLance(leilaoModel), result );
        }
}
