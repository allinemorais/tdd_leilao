package com.leilao.leilao;

import com.leilao.leilao.controller.UsuarioController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Usuario {

    @Test
    public void SetarUsuario()
    {
        UsuarioMoldel usuarioMoldel = new UsuarioMoldel();
        UsuarioController usuarioController = new UsuarioController();
        usuarioMoldel.setId(1);
        usuarioMoldel.setNome("Alline");
        Assertions.assertEquals(usuarioController.AdicionarUsuario(usuarioMoldel),usuarioMoldel);
    }
}
